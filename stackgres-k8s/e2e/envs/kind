#!/bin/sh

KIND_NAME="${KIND_NAME:-kind}"
KIND_NODES="${KIND_NODES:-1}"
KIND_LOCK_PATH="${KIND_LOCK_PATH:-$TARGET_PATH/kind-lock}"
KIND_CONTAINERD_CACHE_PATH="${K8S_CACHE_PATH:-$KIND_CONTAINERD_CACHE_PATH}"
KIND_CONTAINERD_CACHE_RESET="${K8S_CACHE_RESET:-$KIND_CONTAINERD_CACHE_RESET}"
KIND_LOG_PATH="${KIND_LOG_PATH:-$TARGET_PATH/kind-logs}"
KIND_LOG_RESOURCES_POLICY_PATH="${KIND_LOG_RESOURCES_POLICY_PATH:-$TARGET_PATH/kind-apiserver-audit-policy}"
KIND_EXPANDABLE_STORAGE_CLASSNAME="${EXPANDABLE_STORAGE_CLASSNAME:-expandable-sc}"

check_kind_version() {
  if ! kind version | grep -q -F 'kind v0.15.0 '
  then
    echo "kind v0.15.0 is required"
    return 1
  fi
}

get_k8s_env_version() {
  echo "Kind version $(kind version | cut -d ' ' -f 2)"
  echo
}

update_k8s_config() {
  check_kind_version

  mkdir -p "$HOME/.kube"
  if [ "$K8S_FROM_DIND" = true ]
  then
    if docker network ls --format '{{ .Name }}' | grep -q '^kind$'
    then
      local CONTAINER_NAME
      CONTAINER_NAME="$(cat /proc/self/cgroup | grep '^1:name' | cut -d / -f 3)"
      CONTAINER_NAME="${CONTAINER_NAME:-$(hostname)}"
      docker inspect "$CONTAINER_NAME" \
        -f '{{ range $key,$value := .NetworkSettings.Networks }}{{ printf "%s\n" $key }}{{ end }}' \
        | grep -q '^kind$' \
        || docker network connect kind "$CONTAINER_NAME"
    fi
    local KIND_CONTROL_PLANE_IP
    KIND_CONTROL_PLANE_IP="$(docker inspect "$KIND_NAME-control-plane" \
      -f '{{ .NetworkSettings.Networks.kind.IPAddress }}')"
    kind get kubeconfig --name "$KIND_NAME" --internal \
      | sed "s/$KIND_NAME-control-plane/$KIND_CONTROL_PLANE_IP/" \
      > "$HOME/.kube/config-$KIND_NAME"
  else
    kind get kubeconfig --name "$KIND_NAME" \
      > "$HOME/.kube/config-$KIND_NAME"
  fi

  (
  export KUBECONFIG="${KUBECONFIG:-$HOME/.kube/config}"
  if [ -s "$KUBECONFIG" ]
  then
    KUBECONFIG="$HOME/.kube/config-$KIND_NAME":"$KUBECONFIG" \
      kubectl config view --raw > "$HOME/.kube/config-merged"
    mv "$HOME/.kube/config-merged" "$KUBECONFIG"
  else
    mv "$HOME/.kube/config-$KIND_NAME" "$KUBECONFIG"
  fi
  chmod 700 "$KUBECONFIG"
  )

  chmod 700 "$KUBECONFIG"
  # fix for Unable to connect to the server: x509: certificate is valid for <ips>, not <ip>
  kubectl config set "clusters.kind-$KIND_NAME.insecure-skip-tls-verify" --set-raw-bytes true
  kubectl config unset "clusters.kind-$KIND_NAME.certificate-authority-data"
}

reuse_k8s() {
  check_kind_version
  check_kind_image_exists

  try_function update_k8s_config

  if ! kind get clusters | grep -q "^$KIND_NAME$" \
      || ! docker inspect "$KIND_NAME-control-plane" -f '{{ .State.Status }}' \
        | grep -q -F 'running' \
      || ! docker inspect "$KIND_NAME-control-plane" -f '{{ .Config.Image }}' \
        | grep -q -F "kindest/node:v$(get_kind_image "${K8S_VERSION}")"
  then
    echo "Can not reuse kind environment $KIND_NAME"
    reset_k8s
    return
  fi

  echo "Reusing kind environment $KIND_NAME"
}

reset_k8s() {
  check_kind_version
  check_kind_image_exists

  echo "Setting up kind environment $KIND_NAME..."

  if [ -n "$KIND_CONTAINERD_CACHE_PATH" ]
  then
    echo "Setting up kind containerd cache in $KIND_CONTAINERD_CACHE_PATH..."
    if [ "$KIND_CONTAINERD_CACHE_RESET" = true ]
    then
      docker run --rm -v "$KIND_CONTAINERD_CACHE_PATH:/containerd-cache" alpine \
        sh -c 'rm -rf /containerd-cache/*'
    fi
  fi

  if [ -n "$K8S_EXTRA_PORT" ]
  then
    echo "Setting up kind port $K8S_EXTRA_PORT..."
  fi

  delete_k8s
  if [ "$KIND_LOG_RESOURCES" = true ]
  then
    cat << EOF > "$KIND_LOG_RESOURCES_POLICY_PATH"
# Log all requests at the RequestResponse level.
apiVersion: audit.k8s.io/v1
kind: Policy
rules:
- level: RequestResponse
EOF
  fi
  cat << EOF > "$TARGET_PATH/kind-config.yaml"
kind: Cluster
apiVersion: kind.x-k8s.io/v1alpha4
$(
  if [ -n "$KIND_CONTAINERD_CACHE_PATH" ] || \
    docker system info 2> /dev/null | grep -q "Backing Filesystem: \(zfs\|btrfs\)"
  then
    cat << INNER_EOF
containerdConfigPatches:
- |-
$(
  if [ -n "$KIND_CONTAINERD_CACHE_PATH" ]
  then
    cat << INNER_INNER_EOF
 
 root = "/containerd-cache"
INNER_INNER_EOF
  fi
  if docker system info 2> /dev/null | grep -q "Backing Filesystem: zfs" \
    || ([ -d "$KIND_CONTAINERD_CACHE_PATH" ] \
      && df -T "$KIND_CONTAINERD_CACHE_PATH" | tail -n 1 | tr -s ' ' | cut -d ' ' -f 2 | grep -q '^zfs$')
  then
    cat << INNER_INNER_EOF
 [plugins."io.containerd.grpc.v1.cri".containerd]
 snapshotter = "zfs"
INNER_INNER_EOF
  fi
  if docker system info 2> /dev/null | grep -q "Backing Filesystem: btrfs" \
    || ([ -d "$KIND_CONTAINERD_CACHE_PATH" ] \
      && df -T "$KIND_CONTAINERD_CACHE_PATH" | tail -n 1 | tr -s ' ' | cut -d ' ' -f 2 | grep -q '^btrfs$')
  then
    cat << INNER_INNER_EOF
 [plugins."io.containerd.grpc.v1.cri".containerd]
 snapshotter = "btrfs"
INNER_INNER_EOF
  fi
)
INNER_EOF
  fi
)
networking:
  disableDefaultCNI: true
  apiServerAddress: "0.0.0.0"
nodes:
- role: control-plane
$(
  if [ "$KIND_LOG_RESOURCES" = true ]
  then
    cat << INNER_EOF
  kubeadmConfigPatches:
  - |
    kind: ClusterConfiguration
    apiServer:
        # enable auditing flags on the API server
        extraArgs:
          audit-log-path: /var/log/kubernetes/kube-apiserver-audit.log
          audit-policy-file: /etc/kubernetes/policies/audit-policy.yaml
        # mount new files / directories on the control plane
        extraVolumes:
          - name: audit-policies
            hostPath: /etc/kubernetes/policies
            mountPath: /etc/kubernetes/policies
            readOnly: true
            pathType: "DirectoryOrCreate"
          - name: "audit-logs"
            hostPath: "/var/log/kubernetes"
            mountPath: "/var/log/kubernetes"
            readOnly: false
            pathType: DirectoryOrCreate
INNER_EOF
  fi
  if [ -n "$K8S_EXTRA_PORT" ]
  then
    cat << INNER_EOF
  extraPortMappings:
  - containerPort: $(echo "$K8S_EXTRA_PORT" | cut -d : -f 1)
    hostPort: $(echo "$K8S_EXTRA_PORT" | cut -d : -f 2)
    listenAddress: "$(echo "$K8S_EXTRA_PORT" | cut -d : -f 3)"
    protocol: "$(echo "$K8S_EXTRA_PORT" | cut -d : -f 4)"
INNER_EOF
  fi
  if [ -n "$KIND_CONTAINERD_CACHE_PATH" ] \
    || [ "$KIND_LOG" = true ] \
    || [ "$KIND_LOG_RESOURCES" = true ]
  then
    cat << INNER_EOF
  extraMounts:
INNER_EOF
  fi
  if [ -n "$KIND_CONTAINERD_CACHE_PATH" ]
  then
    mkdir -p "$KIND_CONTAINERD_CACHE_PATH"
    cat << INNER_EOF
  - hostPath: $KIND_CONTAINERD_CACHE_PATH
    containerPath: /containerd-cache
INNER_EOF
  fi
  if [ "$KIND_LOG" = true ]
  then
    mkdir -p "$KIND_LOG_PATH"
    cat << INNER_EOF
  - hostPath: $KIND_LOG_PATH
    containerPath: /var/log
INNER_EOF
  fi
  if [ "$KIND_LOG_RESOURCES" = true ]
  then
    cat << INNER_EOF
  - hostPath: $KIND_LOG_RESOURCES_POLICY_PATH
    containerPath: /etc/kubernetes/policies/audit-policy.yaml
    readOnly: true
INNER_EOF
  fi
  for KIND_NODE in $(seq 2 "$KIND_NODES")
  do
    cat << INNER_EOF
- role: worker
INNER_EOF
  done
)
EOF

  try_function flock "$KIND_LOCK_PATH" \
    kind create cluster --name "$KIND_NAME" --config "$TARGET_PATH/kind-config.yaml" \
    --image "kindest/node:v$(get_kind_image "${K8S_VERSION}")"
  if ! "$RESULT" && [ -n "$KIND_CONTAINERD_CACHE_PATH" ] && [ "$KIND_CONTAINERD_CACHE_RESET" != true ]
  then
    echo "Kind failed to create cluster with cache enabled. Resetting cache and retrying!"
    KIND_CONTAINERD_CACHE_RESET=true reset_k8s
    return
  fi

  if [ "$KIND_INSTALL_NFS" = "true" ]
  then
    echo "Setting up NFT tools for kind..."
    kind get nodes --name "$KIND_NAME" \
      | xargs -r -n 1 -I % -P "$E2E_PARALLELISM" sh -ec "
      docker exec '%' sh -c 'DEBIAN_FRONTEND=noninteractive apt-get update -y -qq < /dev/null > /dev/null'
      docker exec '%' sh -c 'DEBIAN_FRONTEND=noninteractive apt-get install -y -qq nfs-common < /dev/null > /dev/null'
      "
  fi

  update_k8s_config

  if [ "${K8S_VERSION%.*}" = 1.12 ]
  then
    echo "Patch coredns to version 1.3.1 (see https://github.com/coredns/coredns/issues/2391)..."
    kubectl patch deployment -n kube-system coredns --type json \
      --patch '[{"op":"replace","path":"/spec/template/spec/containers/0/image","value":"k8s.gcr.io/coredns:1.3.1"}]'
  fi

  echo "Setting up calico for kind..."
  until kubectl get node --template '{{ if (index .items 0).spec.podCIDR }}true{{ end }}' | grep -q 'true'
  do
    sleep 1
  done
  K8S_POD_CIDR="$(kubectl get node --template '{{ (index .items 0).spec.podCIDR }}')"
  if [ "$(echo "$K8S_VERSION" | tr . '\n' | head -n 2 | xargs -I @ printf '%05d' @)" \
      -ge "$(echo "1.22" | tr . '\n' | xargs -I @ printf '%05d' @)" ]
  then
    kubectl create -f https://docs.projectcalico.org/archive/v3.24/manifests/tigera-operator.yaml
    kubectl create -f https://docs.projectcalico.org/archive/v3.24/manifests/custom-resources.yaml
    kubectl patch installations.operator.tigera.io default --type json \
      -p '[{"op":"replace","path":"/spec/calicoNetwork/ipPools/0/cidr","value":"'"$K8S_POD_CIDR"'"}]'
  else
    kubectl apply -f https://docs.projectcalico.org/v3.12/manifests/calico.yaml
    kubectl -n kube-system set env daemonset/calico-node CALICO_IPV4POOL_CIDR=$K8S_POD_CIDR
    kubectl -n kube-system set env daemonset/calico-node FELIX_IGNORELOOSERPF=true
  fi
  echo "...done"
}

delete_k8s() {
  check_kind_version

  if kind get clusters | grep -q "^$KIND_NAME$"
  then
    echo "Deleting kind environment $KIND_NAME..."

    kind delete cluster --name "$KIND_NAME" || true

    echo "...done"
  fi
}

load_image_k8s() {
  check_kind_version

  local IMAGE_ID
  IMAGE_ID="$( (docker inspect --format '{{ .ID }}' "$1" 2>/dev/null || printf unknown) | grep -v '^$')"
  local KIND_IMAGE_ID
  KIND_IMAGE_ID="$( (docker exec "${KIND_NAME}-control-plane" crictl inspecti -o json "$1" 2>/dev/null || printf '{"status": {"id": "unknown"}}') | jq -r '.status.id' | grep -v '^$')"
  if [ "$IMAGE_ID" = unknown ] && [ "$KIND_IMAGE_ID" != unknown ]
  then
    echo "Image $1 already loaded in kind environemnt $KIND_NAME"
    return
  fi
  if [ "$KIND_IMAGE_ID" = "$IMAGE_ID" ] && [ "$IMAGE_ID" != unknown ]
  then
    echo "Image $1 already loaded in kind environemnt $KIND_NAME"
    return
  fi
  kind load docker-image --name "$KIND_NAME" "$1"

  echo "Loaded image $1 in kind environemnt $KIND_NAME"
}

pull_image_k8s() {
  check_kind_version

  local IMAGE_ID
  IMAGE_ID="$( (docker inspect --format '{{ .ID }}' "$1" 2>/dev/null || printf unknown) | grep -v '^$')"
  local KIND_IMAGE_ID
  KIND_IMAGE_ID="$( (docker exec "${KIND_NAME}-control-plane" crictl inspecti -o json "$1" 2>/dev/null || printf '{"status": {"id": "unknown"}}') | jq -r '.status.id' | grep -v '^$')"
  if [ "$IMAGE_ID" = unknown ] && [ "$KIND_IMAGE_ID" != unknown ]
  then
    echo "Image $1 already loaded in kind environemnt $KIND_NAME"
    return
  fi
  if [ "$KIND_IMAGE_ID" = "$IMAGE_ID" ] && [ "$IMAGE_ID" != unknown ]
  then
    echo "Image $1 already loaded in kind environemnt $KIND_NAME"
    return
  fi

  local AUTH
  AUTH="$(jq -r '.auths|to_entries|.[]|.key + "|" + .value.auth' "${HOME}/.docker/config.json" \
    | grep -F "${1%%/*}" | head -n 1 | cut -d '|' -f 2)"
  if [ -n "$AUTH" ]
  then
    docker exec "${KIND_NAME}-control-plane" ctr -n k8s.io i pull --user "$(printf %s "$AUTH" | base64 -d)" "$1" > /dev/null
  else
    docker exec "${KIND_NAME}-control-plane" ctr -n k8s.io i pull "$1" > /dev/null
  fi

  echo "Pulled image $1 in kind environemnt $KIND_NAME"
}

tag_image_k8s() {
  check_kind_version

  docker exec "${KIND_NAME}-control-plane" ctr -n k8s.io images tag --force "$1" "$2"

  echo "Tagged image $1 as $2 in kind environemnt $KIND_NAME"
}

load_certificate_k8s() {
  check_kind_version

  echo "Loading certificate $1 in kind environemnt $KIND_NAME..."

  kind get nodes --name "$KIND_NAME" \
    | xargs -r -n 1 -I % -P "$E2E_PARALLELISM" sh -ec "
    docker cp '$1' '%':/usr/local/share/ca-certificates/validator.crt
    docker exec '%' sh -c update-ca-certificates
    "

  echo "...done"
}

excluded_namespaces() {
  echo "calico-apiserver"
  echo "calico-system"
  echo "default"
  echo "kube-.*"
  echo "local-path-storage"
  echo "tigera-operator"
}

excluded_customresourcedefinitions() {
  echo ".*\.crd\.projectcalico\.org"
  echo ".*\.operator\.tigera\.io"
}

excluded_podsecuritypolicies() {
  echo "calico-.*"
  echo "tigera-operator"
}

excluded_clusterroles() {
  echo "admin"
  echo "calico-.*"
  echo "cluster-admin"
  echo "edit"
  echo "kubeadm:.*"
  echo "local-path-provisioner-role"
  echo "system:.*"
  echo "tigera-operator"
  echo "view"
}

excluded_clusterrolebindings() {
  echo "calico-.*"
  echo "cluster-admin"
  echo "kubeadm:.*"
  echo "local-path-provisioner-bind"
  echo "system:.*"
  echo "tigera-operator"
}

get_k8s_versions() {
  get_kind_images | cut -d @ -f 1 | sed 's/^v//'
}

check_kind_image_exists() {
  try_function get_kind_image "${K8S_VERSION}" > /dev/null 2>&1
  if ! "$RESULT"
  then
    echo "Kind image for k8s version ${K8S_VERSION} not found."
    echo "Available images exists only for versions: $(get_k8s_versions | tr '\n' ' ' | sed 's/ /, /g')"
    return 1
  fi
}

get_kind_image() {
  get_kind_images | sed 's/^v//' | grep "^$1[.@]"
}

get_kind_images() {
  cat << EOF
v1.25.0@sha256:428aaa17ec82ccde0131cb2d1ca6547d13cf5fdabcc0bbecf749baa935387cbf
v1.24.4@sha256:adfaebada924a26c2c9308edd53c6e33b3d4e453782c0063dc0028bdebaddf98
v1.23.10@sha256:f047448af6a656fae7bc909e2fab360c18c487ef3edc93f06d78cdfd864b2d12
v1.22.13@sha256:4904eda4d6e64b402169797805b8ec01f50133960ad6c19af45173a27eadf959
v1.21.14@sha256:f9b4d3d1112f24a7254d2ee296f177f628f9b4c1b32f0006567af11b91c1f301
v1.20.15@sha256:d67de8f84143adebe80a07672f370365ec7d23f93dc86866f0e29fa29ce026fe
v1.19.16@sha256:707469aac7e6805e52c3bde2a8a8050ce2b15decff60db6c5077ba9975d28b98
v1.18.20@sha256:61c9e1698c1cb19c3b1d8151a9135b379657aee23c59bde4a8d87923fcb43a91
EOF
}

create_expandable_storage_class_k8s(){
  cat << EOF | kubectl apply -f - > /dev/null
apiVersion: storage.k8s.io/v1
kind: StorageClass
metadata:
  name: $KIND_EXPANDABLE_STORAGE_CLASSNAME
provisioner: rancher.io/local-path
reclaimPolicy: Delete
volumeBindingMode: WaitForFirstConsumer
allowVolumeExpansion: true
EOF

  printf '%s' "$KIND_EXPANDABLE_STORAGE_CLASSNAME"
}
